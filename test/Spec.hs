{-#LANGUAGE OverloadedStrings #-}
module Main where

import Test.Tasty
import Test.Tasty.HUnit
import Text.StringConvert
import qualified Data.Text as Text
import qualified Data.Text.Lazy as LText

main = defaultMain tests

tests = testGroup "Tests"
    [ testCase "string->string" $ do
        let input = "Hello, world!" :: String
            expected = "Hello, world!" :: String
            actual = s input :: String
        assertEqual "" expected actual
    , testCase "text->ltext" $ do
        let input = "Hello, world!" :: Text.Text
            expected = "Hello, world!" :: LText.Text
            actual = s input :: LText.Text
        assertEqual "" expected actual
    ]
